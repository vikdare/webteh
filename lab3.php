<?php
$datetime1 = DateTime::createFromFormat('d.m.Y', $_POST["selected_date"]);
$datetime2 = new DateTime("now");
$interval = $datetime1->diff($datetime2);
if($interval->invert) {
    $format = 'You will be born in ';
} else {
    $format = 'You are ';
}
$parts = array();
$parts[] = $interval->format('%y') > 0 ? $interval->format('%y years') : '';
$parts[] = $interval->format('%m') > 0 ? $interval->format('%m months') : '';
$parts[] = $interval->format('%d') > 0 ? $interval->format('%d days') : '';
$parts = array_filter($parts, function ($part) {
    return !empty($part);
});
$format .= implode(', ', $parts);
if($interval->invert) {
    echo $interval->format($format);
} else {
    $format .= ' old';
    echo $interval->format($format);
}
