$(document).ready(function () {
    $(function () {
        $("#datepicker").prop('readonly', true);
        $("#datepicker").datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: "1902:2015",
            dateFormat: "dd.mm.yy"
        });
        $("#dateform").submit(function (event) {
            if ($("#datepicker").val() == "") {
                alert("Select you birth date");
            } else {
                $.ajax({
                    type: "POST",
                    url: "lab3.php",
                    data: $("#dateform").serialize()
                }).done(function (data) {
                    $("#age").text(data);
                });
            }
            event.preventDefault();
        });
    });
});