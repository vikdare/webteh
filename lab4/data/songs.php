<?php
$songs = array(
    1 => array(
        "subject" => "Wonderwall",
        "lyrics" => "Today is gonna be the day
            that they're gonna throw it back to you.
            By now you should've somehow
            realized what you gotta do.
            I don't believe that anybody
            feels the way I do about you now.

            Back beat, the word is on the street
            that the fire in your heart is out.
            I'm sure you've heard it all before
            but you never really had a doubt.
            I don't believe that anybody feels
            the way I do about you now.

            And all the roads we have to walk are winding.
            And all the lights that lead us there are blinding.
            There are many things that I would
            like to say to you
            but I don't know how.

            Because maybe
            you're gonna be the one that saves me.
            And after all
            you're my wonderwall.",
    ),
    2 => array(
        "subject" => "Paradise",
        "lyrics" => "When she was just a girl,
                she expected the world,
                but it flew away from her reach,
                and bullets catching her teeth.

                Life goes on,
                it gets so heavy,
                the wheel breaks the butterfly.
                Every tear, a waterfall.
                In the night, the stormy night,
                she closed her eyes.
                In the night, the stormy night,
                away she flied.

                And dream of paradise.",
    ),
    3 => array(
        "subject" => "Shark",
        "lyrics" => "Standing on the world outside,
                    caught up in a love landslide,
                    stuck still, colour blind,
                    hoping for a black and white.
                    Are you gonna be my love?
                    Are you gonna be mine?
                    I feel it falling from the skies above,
                    are you gonna be mine?
                    My wave, my shark, my demon in the dark,
                    the blue tide pulling me under,
                    Or are you my soul, my heart, pull everything apart?
                    Are you gonna, are you gonna be my love?
                    Walking on the clouds unknown,
                    drop down oh I will never go, never go, never go,
                    floating like gravity has grown
                    reach down to a deep hole.",
    ),
);