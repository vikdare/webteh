<?php

class DBHelper {
    public static $db=null;

    public static function getDbValue($key) {
        $dbConfig = $GLOBALS['config']['db'];
        if (empty(self::$db)) {
            self::connect($dbConfig);
        }
        $res = self::query($key, $dbConfig);
        $row = mysqli_fetch_assoc($res);
        return !$row['varval'] ? '' : $row['varval'];
    }

    /**
     * @param $dbConfig
     */
    protected static function connect($dbConfig)
    {
        self::$db = @mysqli_connect(
            $dbConfig['host'],
            $dbConfig['user'],
            $dbConfig['password'],
            $dbConfig['db']
        );
    }

    /**
     * @param $key
     * @param $dbConfig
     * @return bool|mysqli_result
     */
    protected static function query($key, $dbConfig)
    {
        $res = mysqli_query(
            self::$db,
            'SELECT varval FROM ' . $dbConfig['table'] . ' WHERE varkey=' .
            mysqli_real_escape_string(
                self::$db, $key
            )
        );
        return $res;
    }
}