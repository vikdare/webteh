<?php
require_once "data/songs.php";
require_once "data/DBHelper.php";
require_once "config.php";
require_once "lib/TemplateParser.php";
$tpl = new TemplateParser();
$tpl->templateVars['config'] = $GLOBALS['config'];
$tpl->templateVars["foo"] = 1;
$tpl->templateVars["boo"] = rand(0, 1);
if(empty($_GET['id'])) {
    foreach($songs as $id => $onesong) {
        $tpl->templateVars["id".$id] = $id;
        $tpl->templateVars["id".$id] = $id;
        $tpl->templateVars["subject".$id] = $onesong['subject'];
        $tpl->templateVars["lyricsshort".$id] = substr($onesong['lyrics'], 0, 80)." ...";
    }
    $tpl->templateVars['title'] = 'Hello';
    $tpl->render('index.html');
} elseif(in_array($_GET['id'], array_keys($songs))) {
    $tpl->templateVars["id"] = $_GET['id'];
    $tpl->templateVars["subject"] = $songs[$_GET['id']]['subject'];
    $tpl->templateVars["lyrics"] = $songs[$_GET['id']]['lyrics'];
    $tpl->templateVars['title'] = $tpl->templateVars["subject"]. ' lyrics';
    $tpl->render('song.html');
} else {
    $tpl->templateVars["id"] = '';
    $tpl->templateVars["subject"] = 'No song found';
    $tpl->templateVars["lyrics"] = 'No song found';
    $tpl->templateVars['title'] = 'No song found';
    $tpl->render('song.html');
}
