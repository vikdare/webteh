<?php

if (isset($_POST['button'])) {
    $title = substr(htmlspecialchars(trim($_POST['title'])), 0, 1000);
    $mess =  substr(htmlspecialchars(trim($_POST['mess'])), 0, 1000000);
    $name = substr(htmlspecialchars(trim($_POST['name'])), 0, 1000);
    $to = 'viktorymolotova@gmail.com';
    $from = $_POST['email'];
    mail($to, $title, 'From: ' . $name . "\r\n\r\n" . $mess,  $from);
    echo 'Success!';
    header('Refresh: 3; url="index.php"');
}
