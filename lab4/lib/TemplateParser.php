<?php
require_once 'Tags.php';
require_once 'TemplateCompiler.php';

class TemplateParser {
    private $processedFiles = array();

    private $tags = array();

    public $templateFile;

    public $templateVars = array();

    public $replacements = array();

    public function __construct() {
        $this->tags = $GLOBALS['tagconfig'];
    }

    public function render($file) {
        $this->markFileProcessed($file);
        $compiledName = __DIR__.'/compiled/'.$file.'.php';
        $this->templateFile = file_get_contents($file);
        $this->templateFile = $this->makePlainTpl($this->templateFile, $this->tags['FILE']);
        if ($this->isTplChanged($file, $compiledName)) {
            foreach ($this->tags as $tag => $compiler) {
                $this->templateFile = $this->parseSingleTag($this->templateFile, $tag);
            }
            $this->templateFile = '<?php /*' . PHP_EOL . $this->getTplHash() . PHP_EOL . '*/ ?>'
                . PHP_EOL . $this->templateFile;
            file_put_contents($compiledName, $this->templateFile);
        }
        require_once $compiledName;
    }

    public function parseSingleTag($tplContent, $tag) {
        $compilerData = $this->tags[$tag]['compiler'];
        $tplContent = call_user_func_array(
            $compilerData['class'].'::'.$compilerData['handler'],
            array($tplContent, $this)
        );
        return $tplContent;
    }

    public function makePlainTpl($tplContent) {
        $tplContentParsed = $this->parseSingleTag(
            $tplContent,
            'FILE'
        );

        while ($tplContent != $tplContentParsed && !empty($tplContentParsed)) {
            $tplContent = $this->makePlainTpl($tplContentParsed);
        }

        return $tplContent;
    }

    public function markFileProcessed($file) {
        if (!isset($this->processedFiles[$file])) {
            $this->processedFiles[$file] =
                filemtime($file);
        }
    }

    /**
     * @param $file
     * @param $compiledName
     * @return bool
     */
    protected function isTplChanged($file, $compiledName)
    {
        $currentHash = '';
        if (file_exists($compiledName)) {
            $lines = file($compiledName);
            $currentHash = trim($lines[1]);
        }

        return $currentHash != $this->getTplHash();
    }

    /**
     * @return string
     */
    protected function getTplHash()
    {
        $tplHash = md5(serialize($this->processedFiles));
        return $tplHash;
    }
}