<?php

class TemplateCompiler {
    public static function file($content, TemplateParser $tpl) {
        $pattern ='/\{FILE[ |=](.*[^\}])\}/';
        preg_match_all($pattern, $content, $matches);
        foreach ($matches[0] as $key => $match) {
            $filePath = file_get_contents(trim($matches[1][$key], '"\''));
            $content = str_replace(
                $match,
                $filePath,
                $content
            );
            $tpl->markFileProcessed(trim($matches[1][$key], '"\''));
        }
        return($content);
    }

    public static function config($content, TemplateParser $tpl) {
        $pattern ='/\{CONFIG[ |=]"([a-zA-Z0-9_]*)"\}/';
        preg_match_all($pattern, $content, $matches);
        foreach ($matches[0] as $key => $match) {
            $content = str_replace(
                $match,
                '<?php
                echo $this->templateVars[\'config\'][\''.
                trim($matches[1][$key], '"\'').'\'];
                ?>',
                $content
            );
        }
        return($content);
    }

    public static function variable($content, TemplateParser $tpl) {
        $pattern ='/\{VAR[ |=]"([a-zA-Z0-9_]*)"\}/';
        preg_match_all($pattern, $content, $matches);
        foreach ($matches[0] as $key => $match) {
            $content = str_replace(
                $match,
                '<?php
                echo $this->templateVars[\''.trim($matches[1][$key], '"\'').'\'];
                ?>',
                $content
            );
        }
        return($content);
    }

    public static function db($content, TemplateParser $tpl) {
        $pattern ='/\{DB[ |=](.*[^\}])\}/';
        preg_match_all($pattern, $content, $matches);
        foreach ($matches[0] as $key => $match) {
            $content = str_replace(
                $match,
                '<?php
                echo DBHelper::getDbValue(\''.trim($matches[1][$key], '"\'').'\');
                ?>',
                $content
            );
        }
        return($content);
    }

    public static function ifHandler($content, TemplateParser $tpl) {
        $pattern ='/\{IF "([a-zA-Z0-9_]*)"([=!<>]{1,3})"([a-zA-Z0-9_]*)"\}/';
        preg_match_all($pattern, $content, $matches);
        foreach ($matches[0] as $key => $match) {
            $content = str_replace(
                $match,
                '<?php
                if ($this->templateVars[\''.trim($matches[1][$key], '"\'').'\']'
                .trim($matches[2][$key], '"\'')
                .'$this->templateVars[\''.trim($matches[3][$key], '"\'').'\']'
                .') {
                ?>',
                $content
            );
        }
        return($content);
    }

    public static function elseHandler($content, TemplateParser $tpl) {
        $pattern ='/\{ELSE\}/';
        preg_match_all($pattern, $content, $matches);
        foreach ($matches[0] as $key => $match) {
            $content = str_replace(
                $match,
                '<?php
                } else {
                ?>',
                $content
            );
        }
        return($content);
    }

    public static function endifHandler($content, TemplateParser $tpl) {
        $pattern ='/\{ENDIF\}/';
        preg_match_all($pattern, $content, $matches);
        foreach ($matches[0] as $key => $match) {
            $content = str_replace(
                $match,
                '<?php
                }
                ?>',
                $content
            );
        }
        return($content);
    }
}