<?php

$GLOBALS['tagconfig'] = array(
    "FILE" => array(
        'compiler' => array(
            "class" => "TemplateCompiler",
            "handler" => "File"
        ),
    ),
    "CONFIG" => array(
        'compiler' => array(
            "class" => "TemplateCompiler",
            "handler" => "Config"
        ),
    ),
    "VAR" => array(
        'compiler' => array(
            "class" => "TemplateCompiler",
            "handler" => "Variable"
        ),
    ),
    "DB" => array(
        'compiler' => array(
            "class" => "TemplateCompiler",
            "handler" => "Db"
        ),
    ),
    "IF" => array(
        'compiler' => array(
            "class" => "TemplateCompiler",
            "handler" => "IfHandler"
        ),
    ),
    "ELSE" => array(
        'compiler' => array(
            "class" => "TemplateCompiler",
            "handler" => "ElseHandler"
        ),
    ),
    "ENDIF" => array(
        'compiler' => array(
            "class" => "TemplateCompiler",
            "handler" => "EndifHandler"
        ),
    ),
);